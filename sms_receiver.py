import re
import requests
from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse

app = Flask(__name__)

PATTERN = r"\[(?P<service>.+)\](?P<message>[^\[]+)(?P<options>\[.*\])?"
JARVIS_URL = "http://jarvis-service:7777/jarvis"


def extract_service_message_and_options(body):
    result = re.match(PATTERN, body)
    service = result.group('service').strip()
    message = result.group('message').strip()
    options = result.group('options')
    return service, message, options


@app.route("/", methods=['GET', 'POST'])
def capture():
    service, message, options = extract_service_message_and_options(
        request.form['Body'])

    if options:
        data = '{{"service":{}, "message": {}, "options": {}}}'.format(service, message, options)
    else:
        data = '{{"service":{}, "message": {}}}'.format(service, message)
    print(data)
    # jarvis_response = requests.post(JARVIS_URL, data=data)

    resp = MessagingResponse()
    # resp.message('Hello {}, you said: {}'.format(number, message_body))
    return str(resp)


if __name__ == "__main__":
    app.run('0.0.0.0')
