[![Build Status](https://travis-ci.org/sms-jarvis/sms-jarvis.svg?branch=master)](https://travis-ci.org/sms-jarvis/sms-jarvis)
[![Code Health](https://landscape.io/github/sms-jarvis/sms-jarvis/master/landscape.svg?style=plastic)](https://landscape.io/github/sms-jarvis/sms-jarvis/master)

# SMS J.A.R.V.I.S.

Application to handle SMS requests and redirect them to services based on the provided configuration
